
var Bridge = function () {
    this.cb = {}
}

Bridge.prototype.on = function (verb, that, callback) {
    callback || (callback = that) || (that = undefined)
    var array = [ callback, that ]
    this.cb[verb] = this.cb[verb] || []
    this.cb[verb].push(array)
    return array
}

Bridge.prototype.off = function (v, t, c) {
    var cb = this.cb
    for (var k in cb)
    if (cb.hasOwnProperty(k) && (!v || v == k))
    for (var i = 0; i < cb[k].length; i++)
    if ((!t || t == cb[k][i][1]) && (!c || c == cb[k][i][0]))
    this.cb[k].splice(i--, 1)
}

Bridge.prototype.exe = function (verb) {
    var result, cb = this.cb[verb] = this.cb[verb] || []
    var args = Array.prototype.slice.call(arguments, 1)
    cb.forEach(function (cb) { result = cb[0].apply(cb[1], args) })
    return result
}

module.exports = Bridge
