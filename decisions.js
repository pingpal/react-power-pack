var React = require('react')
var $ = require('jquery')

var m = require('./mergeStyles')
var f = require('./findDOMNode')
var B = require('./callBridge')

var styles = {}

styles.control = {
    position: 'relative',
    zIndex: 1,
    height: 50,
    background: '#fff',
    border: 'solid 1px #eee'
}

styles.label = {
    position: 'relative',
    height: 50,
    padding: '15px 0 0 20px',
    color: '#999',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    cursor: 'pointer'
}

styles.label_selected = {
    color: '#333'
}

styles.input = {
    position: 'absolute',
    top: 0,
    left: 0,
    height: 48,
    padding: '1px 0 0 20px',
    color: '#333',
    border: 'none',
    outline: 'none',
}

styles.button = {
    float: 'right',
    position: 'relative',
    top: -1,
    right: -1,
    width: 50,
    height: 50,
    marginLeft: 20,
    marginBottom: -1,
    background: '#079DD9',
    cursor: 'pointer'
}

styles.button_press = {
    background: '#037dca'
}

styles.arrow = {
    width: 0,
    height: 0,
    margin: '22px auto 0 auto',
    borderLeft: '7px solid transparent',
    borderRight: '7px solid transparent',
    borderTop: '7px solid #fff'
}

styles.options = {
    background: 'red',
    // position: 'relative',
    // top: -1,
    border: 'solid 1px #eee',
    borderTop: 'none',
    borderBottom: 'none',
    background: '#fff',
    boxShadow: '0 3px 10px rgba(0, 0, 0, 0.16)'
}

styles.option = {
    height: 50,
    width: '100%',
    padding: '15px 20px 0 20px',
    borderBottom: 'solid 1px #eee',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    cursor: 'pointer'
}

styles.option_hover = {
    background: '#f5f5f5'
}

styles.select = {
    position: 'relative',
    zIndex: 100001,
    height: 50,

    userDrag: 'none',
    userSelect: 'none',
    MozUserDrag: 'none',
    MozUserSelect: 'none',
    WebkitUserDrag: 'none',
    WebkitUserSelect: 'none'
}

styles.backdrop = {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    background: 'transparent',
    zIndex: 100000,

    background: 'rgba(255, 0, 0, 0)'
}

var Control = React.createClass({
    getInitialState: function () {
        return { press: false, selected: null }
    },
    componentWillUnmount: function () {
        this.props.bridge.off(null, this)
    },
    componentWillMount: function() {
        this.selectionDidOccur(this.props.selected || { value: -1, name: '' })

        this.props.bridge.on('select', this, function (selected) {
            this.selectionDidOccur(selected)
        })

        this.props.bridge.on('-grab-selected', this, function () {
            return this.state.selected
        })
    },
    selectionDidOccur: function (selected) {
        this.setState({ selected: selected })
    },
    stopPropagation: function (e) {
        e.stopPropagation()
    },
    mouseUp: function () {
        this.setState({ press: false })
        this.props.onButtonUp()
    },
    mouseDown: function () {
        this.setState({ press: true })
    },
    mouseOut: function () {
        this.setState({ press: false })
    },
    keyUp: function (e) {
        var name = e.target.value

        if (this.state.selected.name != name) {
            this.setState({ selected: { value: -1, name: name  } })
            this.props.onChange(name)
        }
    },
    render: function() {

        var text = name = this.state.selected.name
        name = name || this.props.hint

        var labelStyle = m(styles.label, text && styles.label_selected)
        var buttonStyle = m(styles.button, this.props.styleBtn, this.state.press && styles.button_press, this.state.press && this.props.styleBtnPress)
        var inputStyle = m(styles.input, { width: this.width })

        var input = this.props.open && <input ref="input" defaultValue={text} style={inputStyle} onKeyUp={this.keyUp} onClick={this.stopPropagation}/>

        return (
            <div style={m(styles.control, !this.props.open && this.props.styleCtrl)}>
                <div style={buttonStyle} onMouseUp={this.mouseUp} onMouseDown={this.mouseDown} onMouseOut={this.mouseOut}>
                    <div style={m(styles.arrow, this.props.styleArrow)}></div>
                </div>
                <div ref="label" style={labelStyle} onClick={this.props.onButtonUp}>
                    {name}
                    {input}
                </div>
            </div>
        )
    },
    componentDidUpdate: function () {
        this.width = f(this, 'label').offsetWidth
        this.backdrop = this.backdrop || $()

        if (this.props.open) {
            var el = f(this, 'input')

            el.focus()
            var val = el.value
            el.value = ''
            el.value = val

            // var style = 'position:absolute;top:0;right:0;bottom:0;left:0;background:transaprent;z-index:'
            //
            // style += ';background: rgba(255, 0, 0, .1)'
            //
            // var backdrop = $('<div style="' + style + '"></div>')
            // backdrop.on('click', function () { this.props.onButtonUp() }.bind(this))
            //
            // this.backdrop.remove()
            // this.backdrop = this.backdrop.add(backdrop)
            // $(document.body).append(backdrop)

        } else {
            // this.backdrop.remove()
        }
    }
})

var Option = React.createClass({
    getInitialState: function () {
        return { hover: false }
    },
    onIn: function () {
        this.setState({ hover: true })
    },
    onOut: function () {
        this.setState({ hover: false })
    },
    onClick: function () {
        this.props.onChange(this.props.name)
        this.props.onSelect(this.props.value)
    },
    render: function () {
        var optionStyle = m(styles.option, this.props.styleOptn, this.state.hover && styles.option_hover, this.state.hover && this.props.styleOptnHover)

        return (
            <div style={optionStyle} data-value={this.props.value} onMouseOver={this.onIn} onMouseOut={this.onOut} onClick={this.onClick}>
                {this.props.name}
            </div>
        )
    }
})

var Options = React.createClass({
    render: function () {
        var props = this.props
        var options = this.props.options.map(function (option) {
            return <Option key={option.value} {...this.props} {...option}/>
        }.bind(this))

        return this.props.options.length ? <div style={styles.options}>{options}</div> : null
    }
})

var Select = React.createClass({
    getInitialState: function () {
        return { open: false }
    },
    toggleOptions: function () {
        this.setState({ open: !this.state.open })
    },
    onSelect: function (value) {
        this.setState({ open: false })
        this.props.onSelect(value)
    },
    render: function () {
        var options = this.state.open && <Options {...this.props } onSelect={this.onSelect}/>
        return (
            <div>
                <div style={styles.select}>
                    <Control {...this.props} open={this.state.open} onButtonUp={this.toggleOptions}/>
                    {options}
                </div>
                {this.state.open && <div style={styles.backdrop} onClick={this.toggleOptions}></div>}
            </div>
        )
    }
})

var Decisions = React.createClass({
    getInitialState: function () {
        return { selected: this.props.selected, bridge: this.props.bridge || new B }
    },
    componentWillUnmount: function () {
        this.state.bridge.off(null, this)
    },
    componentWillMount: function() {
        this.state.bridge.on('grab-selected', this, function () {
            var selected = this.props.bridge.exe('-grab-selected')

            if (selected.value < 0) {
                for (var i = 0; i < this.props.options.length; i++) {
                    if (this.props.options[i].name == selected.name) {
                        selected = this.props.options[i]
                    }
                }
            }

            return selected
        })
    },
    onSelect: function (value) {
        for (var i = 0; i < this.props.options.length; i++) {
            if (this.props.options[i].value == value) {
                this.setState({ selected: this.props.options[i] })
                this.props.onSelect(this.props.options[i])
                this.props.bridge.exe('select', this.props.options[i])
            }
        }
    },
    render: function () {
        return <Select {...this.props} selected={this.state.selected} onSelect={this.onSelect} bridge={this.state.bridge}/>
    }
});

module.exports = Decisions
