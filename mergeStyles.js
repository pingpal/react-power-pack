var objectAssign = require('react/lib/Object.assign');

function mergeStyles() {
    var styles = {};

    for (var i = 0; i < arguments.length; i++) {
        if (arguments[i]) {
            objectAssign(styles, arguments[i])
        }
    }

    return styles;
}

module.exports = mergeStyles
