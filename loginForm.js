
var React = require('react')
var $ = require('jquery')

var m = require('./mergeStyles')
var f = require('./findDOMNode')
var B = require('./callBridge')

var styles = {}

styles.box = {
    display: 'block',
    width: 360,
    margin: '0 auto',
    background: '#fff',

    WebkitAnimationDuration: '.7s',
    MozAnimationDuration: '.7s',
    AnimationDuration: '.7s'
}

styles.row = {
    position: 'relative'
}

styles.btn = {
    display: 'block',
    textDecoration: 'none',
    padding: 15,
    background: '#079DD9',
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    cursor: 'pointer'
}

styles.btn_press = {
    background: '#037dca'
}

styles.input = {
    padding: '30px 10px 30px 50px',
    border: 'none',
    borderRadius: 0,
    background: 'transparent'
}

styles.inpUp = {
    borderBottom: 'solid 1px #eee'
}

styles.icon = {
    position: 'absolute',
    top: 23,
    left: 20,
    color: '#999'
}

styles.noglow = {
    outline: 'none',
    // border: 'none !important',

    shadow: 'none !important',
    MozBoxShadow: 'none !important',
    WebkitBoxShadow: 'none !important'
}

styles.submit = {
    position: 'absolute',
    top: '-9999px'
}

var HoverButton = React.createClass({
    getInitialState: function () {
        return { hover: false }
    },

    mouseDown: function () {
        this.setState({ hover: true })
    },

    mouseUp: function () {
        this.setState({ hover: false })
    },

    render: function() {

        var style = m(/*styles.row,*/ styles.btn, this.props.styleBtn, this.state.hover && styles.btn_press, this.state.hover && this.props.styleBtnPress)

        return <a style={style} onClick={this.props.handleLogin} onMouseDown={this.mouseDown} onMouseUp={this.mouseUp}>Sign in</a>
    }
});

var LoginForm = React.createClass({
    shakeBox: function () {
        var ends = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend'
        $('#form-box').addClass('animated shake').one(ends, function () {
            $(this).removeClass('animated shake')
            this.offsetWidth = this.offsetWidth
        })
    },
    handleLogin: function (e) {
        e.preventDefault()

        var username = f(this, 'username').value.trim()
        var password = f(this, 'password').value.trim()

        if (!username/* || !password*/) {
            this.shakeBox()
            return
        }

        f(this, 'password').value = f(this, 'username').value = ''

        this.props.handleLogin(username, password) || this.shakeBox()
    },
    render: function () {
        return (
            <form onSubmit={this.handleLogin} style={m(styles.box, this.props.styleBox)} id="form-box">
                <div style={styles.row}>
                    <span style={styles.icon} className="fa fa-user"></span>
                    <input ref="username" type="username" style={m(styles.noglow, styles.input, styles.inpUp, this.props.styleInput, this.props.styleInpUp)} className="form-control" placeholder="Username"/>
                </div>
                <div style={styles.row}>
                    <span style={styles.icon} className="fa fa-unlock-alt"></span>
                    <input ref="password" type="password" style={m(styles.noglow, styles.input, this.props.styleInput)} className="form-control" placeholder="Password"/>
                </div>
                <HoverButton {...this.props} handleLogin={this.handleLogin} />
                <input type="submit" style={styles.submit}/>
            </form>
        );
    }
});

module.exports = LoginForm
