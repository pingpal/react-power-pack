var React = require('react');

function findDOMNode (that, ref) {
    return React.findDOMNode(that.refs[ref])
}

module.exports = findDOMNode
